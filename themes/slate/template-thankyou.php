<?php
/**
 * Template Name: GW Hospital Thank You
 * Description: Page template for GW Hospital.
 */

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

//GRABS MOST RECENT POSTS
$articleargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['posts'] = Timber::get_posts( $articleargs );

Timber::render( array( 'templates/gw-hospital-thankyou.twig'), $context );