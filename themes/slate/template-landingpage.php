<?php
/**
 * Template Name: GW Hospital LP
 * Description: Page template for GW Hospital LP.
 */


$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

$today = date('Ymd');
$todayTime = date('Y-m-d H:i:s');

//GRABS MOST RECENT POSTS
$articleargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['posts'] = Timber::get_posts( $articleargs );

Timber::render( array( 'templates/gw-hospital-lp.twig'), $context );
