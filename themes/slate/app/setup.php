<?php
/**
 * Setup file used to register theme features.
 */



/**
 * Register Slate Menus.
 */

function sl_register_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
	  'custom-menu' => __( 'Custom Menu' ),
	  'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'sl_register_menus' );


/**
 * Register Slate Sidebar.
 */
function sl_widgets_sidebar_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'side_1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'sl_widgets_sidebar_init' );

/**
 * Register ACF Theme Options Page.
 */
if( function_exists('acf_add_options_page') ) {
	
    acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
		'menu_slug'		=> 'theme-header-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
		'menu_slug'		=> 'theme-footer-settings',
	));
	
}

//Add ACF Flexible Content Title Filter
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_fields_flexible_content_layout_title', 10 , 4);
function my_acf_fields_flexible_content_layout_title( $title, $field, $layout, $i ) {

    // Load text sub field
    if( $text = get_sub_field('module_title')) {
        $title .= ': <strong>' . esc_html($text) . '</strong>';
    }
    return $title;
}

add_filter('acf/load_field/type=repeater', 'make_it_collapse');
	function make_it_collapse($field) {
		$field['collapsed'] = 'card';
		return $field;
	}

/**
 * Slate Admin Styles
 */



