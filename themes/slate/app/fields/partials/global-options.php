<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsglobal = new FieldsBuilder('global_options');

$optionsglobal
    ->setLocation('options_page', '==', 'theme-general-settings')

    //Address Fields
    ->addGroup('address', [
        'wrapper' => ['width' => 50]
    ])        
        ->addText('street_address', [
            'label' => 'Street Address',
            'ui' => $config->ui
        ])
        ->setInstructions('The street address for the facility. This will be used throughout the Site.')

        ->addText('city', [
            'label' => 'City',
            'ui' => $config->ui
        ])
        ->setInstructions('Put the City i.e. Nashville')

        ->addText('state', [
            'label' => 'State',
            'ui' => $config->ui
        ])
        ->setInstructions('Put the State here i.e. TN')

        ->addText('zip_code', [
            'label' => 'Zip Code',
            'ui' => $config->ui
        ])
        ->setInstructions('The Zip Code of the facility')
    ->endGroup()

    //SMS Phone Number Field
    ->addGroup('text_messenger', [
        'wrapper' => ['width' => 50]
    ])
        ->addText('text_messenger', [
            'label' => 'SMS number',
            'ui' => $config->ui
        ])
    ->setInstructions('Add the number for the SMS messenging.')
    ->endGroup();

return $optionsglobal;